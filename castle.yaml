links:
  always:
    - castle_courtyard south-of castle_entryway_hall_1
    - castle_entryway_hall_1 south-of castle_entryway_hall_2
    - castle_entryway_hall_2 south-of castle_great_hall
    - castle_chapel east-of castle_entryway_hall_1
    - castle_undercroft west-of castle_entryway_hall_1
    - castle_kitchen west-of castle_entryway_hall_2
    - castle_solar east-of castle_entryway_hall_2
    - castle_ladder_downward west-of-oneway castle_undercroft
    - caves_deep_back west-of-oneway castle_ladder_downward
    - castle_ladder_upward_1 east-of-oneway caves_deep_back
    - castle_ladder_upward_2 east-of-oneway castle_ladder_upward_1
    - castle_undercroft east-of-oneway castle_ladder_upward_2

locations:
  castle_courtyard:
    region: castle
    prompt:
      - condition:
          type: always
        text: |-
          You stand in the courtyard of the castle,{.5} a few wooden posts lining the area where a fence would have been.{1} The stone castle overshadows the courtyard.{1}

          {North} is a large re-inforced door;{.5} the entrance to the castle.{1}
          {South} a grassy path leads to the forest.

  castle_entryway_hall_1:
    region: castle
    prompt:
      - condition:
          type: always
        text: |-
          In the entryway of the castle,{.5} you feel a chill as cool air brushes past you.{1} A few doors lead off into other rooms.{1}

          {South} is a half open re-inforced door,{.5} leading to the courtyard.{1}
          {East} is an open arch entryway,{.5} leading into what appears to be a small chapel.{1}
          {West} appears to be spiral staircase downward.{1} Leading to what, you wonder?{1}
          {North} the hallway continues.

  castle_entryway_hall_2:
    region: castle
    prompt:
      - condition:
          type: always
        text: |-
          It's hard to see in the hallway,{.5} due to the lack of natural light.{1} The long corridor has rooms leading off it.{1}

          {North} is a large door,{.5} with the following etched into the stone above:{.5} {written}great hall{/written}.{1} Unfortunately it is sealed off,{.5} a large iron bar preventing the door from being opened.{1}

          {South} the hallway continues.{1}
          {East} a spiral staircase leads upwards.{1}
          {West} is an entryway with a half broken door,{.5} sitting only on one hinge.{1} It eminates with the smell of mould.{1}

  castle_chapel:
    region: castle
    prompt:
      - condition:
          type: always
        text: |-
          You stand in the middle of a chapel,{.5} room filled with polished marble and ornate decorations.{1} Along the main back wall,{.5} light shines through a large stained glass mural depicting a figure,{.5} though you do not recognise them.{1}

          Despite the age of the room,{.5} it is in reasonably good condition.

          {West} is the archway back into the main entryway.
    inspectables:
      mural:
        aliases:
          - stained glass
          - back wall
        prompt:
          - condition:
              type: always
            text: |-
              The stained glass mural depicts a large humanoid figure looking downward into the chapel.{1} You notice the figure appears to have protrusions from their head,{.5} almost like horns.{1}

              Taking a closer look,{.5} you feel as though the mural is staring at you.{1} Perhaps it's best not to linger.{.5}
  castle_kitchen:
    region: castle
    prompt:
      - condition:
          type: always
        text: |-
          You stand in an old kitchen,{.5} mould growing over the old iron bench.{1} The smell of the mould is absolutely foul.{1}

          The mould growth leads to the corner of the room,{.5} where a large mound of fungus grows.{1}

          {East} is the broken door back into the main entryway.
    inspectables:
      bench:
        aliases:
          - mould
        prompt:
          - condition:
              type: always
            text:
              You hesitantly approach the mould,{.5} your eyes watering.{1} As you open the over door,{.5} slimey water leaks out and spills all over the floor.{.5} You leap back in response.
      corner:
        aliases:
          - mound
          - fungus
        prompt:
          - condition:
              type: always
            text:
              The mound of fungus is several feet wide,{.5} and is covered in hundreds of little black spots.{1} You notice they almost look like eyes.{1}

              As you lean in to get a closer look,{.5} the mound slightly vibrates.{.3}.{.3}.{.3} from the cool breeze that just came in through the kitchen door.
  castle_solar:
    region: castle
    no_spawn: true
    prompt:
      - condition:
          type: always
        text: |-
          At the top of the staircase,{.5} the room opens up into a dining area.{1} The floor is lined with dust,{.5} and cobwebs fill the corners at the top of the room.{1}

          A large stone table commands the center of the room,{.5} worn and warped from it's use centuries ago.{1}

          There is a window {east} and {north},{.5} each overlooking the surrounding forest.{1}

          {West} the stairway leads downward.
    inspectables:
      window:
        aliases:
          - forest
        prompt:
          - condition:
              type: always
            text: |-
              You peer out one of the windows.{1} Far in the distance,{.5} you can see beyond the forest to the ocean.{1} The ocean sky is filled with an approaching storm,{.5} lighting occasionally snaking it's way down and striking the water.{1}
      table:
        prompt:
          - condition:
              type: always
            text: |-
              It is a rather uninteresting stone table.{1} With no chairs to be seen,{.5} you see very little use for it.

  castle_undercroft:
    region: castle
    prompt:
      - condition:
          type: always
        text: |-
          Standing in a small undercroft,{.5} you almost have to crouch due to the low height of the room.{1} It's dark,{.5} humid{.5} and the air is terribly stale.{1}

          In the middle of the room,{.5} there is a trap door.{1}

          {West}, you can lift the trap door and head downward.
          {East} a spiral staircase leads upward.

  castle_ladder_downward:
    region: castle
    no_spawn: true
    prompt:
      - condition:
          type: always
        text: |
          You open the trap door, and start climbing down the ladder below.{1} It's wet and slippery.{1}

          .{.5}.{.5}.{.5} Your foot slips and you fall.{.1}.{.1}.{.1}.{.1}.{.1}.{.1}.{.1}.{.1}.{.1}

          With a painful thud,{.5} you land on the hard rocky ground.{1} Damp air surrounds you as you struggle to get up.{1}

          Disorientated, your vision slowly adjusts.{1} You can see that you are in what appears to be a cave,{.5} and only a sliver of light is visible.{1}

  castle_ladder_upward_1:
    region: castle
    no_spawn: true
    prompt:
      - condition:
          type: always
        text: |-
          In the small space you can see a tunnel upwards, dark and forboding.{1} On the cave wall are metal rungs,{.5} rusted and wet,{.5} forming a ladder leading upward.{1}

          {East} are the metal rungs that you can use to climb upward.

  castle_ladder_upward_2:
    region: castle
    no_spawn: true
    prompt:
      - condition:
          type: always
        text: |-
          You bump your head against a trap door.{1} Holding on,{.5} you realise it is too late to turn back now.{1}

          {East}, you can open the trap door.
