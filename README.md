# A Momentary Meeting Database

You can edit `database.yaml` and then type `reload` once you're logged in to refresh from the repository.

In development, it always reads the local file and doesn't pull from the repository until you type `reload` for the first time. In production, it always loads the database from GitLab during startup.